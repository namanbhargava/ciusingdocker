FROM java:8-jdk-alpine
ADD /SpringNewStarterApp/target/SpringNewStarterApp-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
