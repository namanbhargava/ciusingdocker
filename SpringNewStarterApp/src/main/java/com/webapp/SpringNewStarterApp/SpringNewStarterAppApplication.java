package com.webapp.SpringNewStarterApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringNewStarterAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringNewStarterAppApplication.class, args);
	}

}
