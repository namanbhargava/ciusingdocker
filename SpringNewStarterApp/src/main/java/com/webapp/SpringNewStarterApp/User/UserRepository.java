package com.webapp.SpringNewStarterApp.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserPojo,String> {
    UserPojo findUserPojoByEmail(String email);
}
