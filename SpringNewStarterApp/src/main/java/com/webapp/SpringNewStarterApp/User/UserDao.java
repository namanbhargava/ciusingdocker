package com.webapp.SpringNewStarterApp.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDao {

    @Autowired
    UserRepository userRepository;

    public void addUser(UserPojo userobj)
    {
        userRepository.save(userobj);
    }
}
