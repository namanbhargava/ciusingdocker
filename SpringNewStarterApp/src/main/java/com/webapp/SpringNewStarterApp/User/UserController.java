package com.webapp.SpringNewStarterApp.User;

import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class UserController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserDao userdao;

    @RequestMapping(value="/hello")
    public String sayHello(HttpServletRequest request, HttpServletResponse response) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "hello");
        return jsonObject.toString();
    }

    @RequestMapping(value="/user/register", method = RequestMethod.POST)
    public String addUser(@RequestBody UserPojo userpojo){
        UserPojo up = new UserPojo();
        up.setId(userpojo.getId());
        up.setName(userpojo.getName());
        up.setEmail(userpojo.getEmail());
        up.setPassword(bCryptPasswordEncoder.encode(userpojo.getPassword()));
        userdao.addUser(up);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "User added successfully");
        return jsonObject.toString();
    }
}
